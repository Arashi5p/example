module gitlab.eldorado.ru/golang/pgx-bulk

go 1.16

require (
	github.com/jackc/pgx/v4 v4.10.1
	github.com/lib/pq v1.9.0
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.7.0
	gitlab.eldorado.ru/golang/pgx-connect v0.1.7
)
