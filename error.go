package bulk

type Error string

func (s Error) Error() string {
	return string(s)
}

const (
	OK string = "OK"

	Canceled        Error = "Canceled"
	Unknown         Error = "Unknown"
	InvalidArgument Error = "InvalidArgument: %s"
)
