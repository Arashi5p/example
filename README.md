# Bulk

Сервис для массового обновления записей в базе данных. Работает только с postrgres&

Пример использования

##### Создание сервиса
```
bulkService := bulk.NewBulkService(ctx, bulk.Config{
		Conn: bulk.DataBase{
			DataBaseName: "bulk",
			User:         "postgres",
			Password:     "postgres",
			Host:         "localhost",
			Port:         5432,
			Secure:       "disable",
		},
		TableBulkParams: bulk.TableParam{
			Name:     "bulk_test",
			PkColumn: "city_id",
			Fields: map[string]string{
				"Name":      "name",
				"CityId":    "city_id",
				"Comment":   "comment",
				"CreatedAt": "created_at",
			},
			UniqueConstraint: []string{"name"},
		},
	})
```

##### Создали новую версию набора данных
```
dv, err := bulkService.CreateDataVersion(ctx, "bulk_test", "city_id=1234567&name=asd")
```


##### Добавление данных
```
models := make([]interface{}, 0)
for _, v := range cities {
    models = append(models, v)
}
m, err := bulkService.InsertDataVersion(ctx, uint(dv.DataVersionId), models, "bulk_test")
```

##### Применить изменения
```
bulkService.ApplyDataVersion(ctx, uint(dv.DataVersionId), "bulk_test")
```
