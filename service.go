package bulk

import (
	"context"
	"crypto/md5"
	"database/sql"
	"fmt"
	"os"
	"os/signal"
	"reflect"
	"strings"
	"syscall"
	"time"

	"github.com/jackc/pgx/v4"
	"github.com/pkg/errors"
	pgx_connection "gitlab.eldorado.ru/golang/pgx-connect"
)

const (
	bulkVersionField      = "data_version_bulk"
	defaultClearDelayHour = 12
)

type resource interface {
	Query(string, ...interface{}) (*sql.Rows, error)
}

type DataVersion struct {
	DataVersionId uint32 `json:"data_version_id,omitempty"`
	Name          string `json:"name,omitempty"`
}

type Model map[string]interface{}
type Models []Model

type BulkService struct {
	Conn        pgx_connection.Connection
	tableParams map[string]tableBulkParam
	clearDelay  uint
	ctx         context.Context
}

type Config struct {
	TableBulkParams []TableParam
	ClearDelayHour  uint
}

// DataBase - Connections settings
type DataBase struct {
	User         string
	Password     string
	Host         string
	Port         int
	DataBaseName string
	Secure       string
}

func NewBulkService(config Config, conn pgx_connection.Connection) (*BulkService, error) {

	if config.ClearDelayHour == 0 {
		config.ClearDelayHour = defaultClearDelayHour
	}

	bulkService := &BulkService{
		Conn:        conn,
		tableParams: make(map[string]tableBulkParam),
		clearDelay:  config.ClearDelayHour,
	}
	for i := range config.TableBulkParams {

		if len(config.TableBulkParams[i].UniqueConstraint) == 0 {
			return nil, errors.Wrap(InvalidArgument, fmt.Sprint("empty UniqueConstraint in $s ", config.TableBulkParams[i].Name))
		}

		bulkService.tableParams[config.TableBulkParams[i].Name] = tableBulkParam{
			tableName:        config.TableBulkParams[i].Name,
			pkColumn:         config.TableBulkParams[i].PkColumn,
			fields:           config.TableBulkParams[i].Fields,
			uniqueConstraint: config.TableBulkParams[i].UniqueConstraint,
			hasher:           md5.New(),
		}
	}

	ctx := context.Background()
	go bulkService.loop(ctx)
	ctx = context.Background()

	return bulkService, nil
}

func (s BulkService) CreateDataVersion(ctx context.Context, modelCode string, deletedQuery string) (version *DataVersion, err error) {
	conn, ctx, err := s.Conn.Begin(ctx)
	defer func(ctx context.Context) {
		if err != nil {
			s.Conn.Rollback(ctx)
			return
		}
		err = s.Conn.Commit(ctx)
		if err != nil {
			err = errors.Wrap(err, "createTableBulk commit")
			return
		}
	}(ctx)

	if modelCode == "" {
		err = errors.Wrap(InvalidArgument, "Model codes are not specified")
		return
	}

	if !s.isTableExist(ctx, "data_version") {
		ct := `
			CREATE TABLE data_version
			(
				data_version_id serial PRIMARY KEY,
				"time"          timestamp without time zone,
				"delete"        boolean DEFAULT false
			);`
		_, err = conn.Exec(ctx, ct)
		if err != nil {
			return
		}
	}

	versionID := 0
	row := conn.QueryRow(ctx, `INSERT INTO data_version (time) VALUES ($1) RETURNING data_version_id`, time.Now())
	if err := row.Scan(&versionID); err != nil {
		return nil, err
	}

	err = s.createTableBulk(ctx, modelCode, uint(versionID), deletedQuery)
	if err != nil {
		return
	}

	return &DataVersion{DataVersionId: uint32(versionID)}, nil
}

func (s BulkService) createTableBulk(ctx context.Context, modelCode string, dataVersionId uint, deletedQuery string) (err error) {
	conn, ctx, err := s.Conn.Begin(ctx)
	defer func(ctx context.Context) {
		if err != nil {
			s.Conn.Rollback(ctx)
			err = errors.Wrap(err, "createTableBulk")
			return
		}
		err = s.Conn.Commit(ctx)
		if err != nil {
			err = errors.Wrap(err, "createTableBulk commit")
			return
		}
	}(ctx)

	tableParams := s.tableParams[modelCode]
	tableNameNew := tableParams.tableNameNew(dataVersionId)

	if s.isTableExist(ctx, tableNameNew) {
		return errors.Wrap(Canceled, "The temporary table has already been created")
	}

	if !s.isTableExist(ctx, tableParams.tableName) {
		return errors.Wrap(Canceled, "The main table does not exist")
	}

	queryMaxRes := 0
	sequenceNameNew := tableParams.sequenceNameNew(dataVersionId)
	sequenceForAlignmentName := tableParams.sequenceForAlignmentName(dataVersionId)
	sequenceVersionBulkName := tableParams.sequenceVersionBulkName(dataVersionId)

	_, err = conn.Exec(ctx, fmt.Sprintf("CREATE TABLE %v (LIKE %v INCLUDING ALL)", tableNameNew, tableParams.tableName))
	if err != nil {
		return err
	}

	if tableParams.pkColumn != "" {
		err = conn.QueryRow(ctx, fmt.Sprintf("SELECT MAX(%v) + 1 FROM %v", tableParams.pkColumn, tableParams.tableName)).Scan(&queryMaxRes)
		if err != nil {
			return err
		}

		_, err = conn.Exec(ctx, fmt.Sprintf("CREATE SEQUENCE %v START %v", sequenceNameNew, queryMaxRes))
		if err != nil {
			return err
		}

		_, err = conn.Exec(ctx, fmt.Sprintf("CREATE SEQUENCE %v START %v", sequenceForAlignmentName, queryMaxRes))
		if err != nil {
			return err
		}

	}

	_, err = conn.Exec(ctx, fmt.Sprintf("CREATE SEQUENCE %v", sequenceVersionBulkName))
	if err != nil {
		return err
	}

	_, err = conn.Exec(ctx, fmt.Sprintf("ALTER TABLE %v ADD %v integer NULL", tableNameNew, bulkVersionField))
	if err != nil {
		return err
	}

	_, err = conn.Exec(ctx, fmt.Sprintf("ALTER TABLE %v ADD %v boolean DEFAULT false", tableNameNew, tableParams.UpdatedColumnName()))
	if err != nil {
		return err
	}

	_, err = conn.Exec(ctx, fmt.Sprintf("ALTER TABLE %v ADD %v boolean DEFAULT false", tableNameNew, tableParams.DeletedColumnName()))
	if err != nil {
		return err
	}

	if tableParams.pkColumn != "" {
		cn, err := s.Conn.GetConstraintName(ctx, tableNameNew, []string{tableParams.pkColumn})
		if err != nil {
			return err
		}
		if cn != "" {
			_, err = conn.Exec(ctx, fmt.Sprintf("ALTER TABLE %v DROP CONSTRAINT %v", tableNameNew, cn))
			if err != nil {
				return err
			}
		}
		_, err = conn.Exec(ctx, fmt.Sprintf("ALTER TABLE %v ALTER COLUMN %v SET DEFAULT nextval('%v')", tableNameNew, tableParams.pkColumn, sequenceNameNew))
		if err != nil {
			return err
		}

		_, err = conn.Exec(ctx, fmt.Sprintf("ALTER TABLE %v ALTER %v SET NOT NULL", tableNameNew, tableParams.pkColumn))
		if err != nil {
			return err
		}
	}

	originalUniqueConstraint, err := s.Conn.GetConstraintName(ctx, tableNameNew, tableParams.uniqueConstraint)
	if originalUniqueConstraint != "" {
		_, err = conn.Exec(ctx, fmt.Sprintf("ALTER INDEX %v RENAME TO %v", originalUniqueConstraint, tableParams.uniqueConstraintNew(dataVersionId)))
		if err != nil {
			return err
		}
	}

	_, err = conn.Exec(ctx, fmt.Sprintf("INSERT INTO %v as t SELECT * FROM %v", tableNameNew, tableParams.tableName))
	if err != nil {
		return err
	}

	_, err = conn.Exec(ctx, fmt.Sprintf("UPDATE %v SET %v = true %s", tableNameNew, tableParams.DeletedColumnName(), PrepareQueryWhere(deletedQuery)))
	if err != nil {
		return err
	}

	return err
}

func (s BulkService) isTableExist(ctx context.Context, tableName string) (result bool) {
	conn, ctx, err := s.Conn.Begin(ctx)
	defer func(ctx context.Context) {
		if err != nil {
			s.Conn.Rollback(ctx)
			result = false
			return
		}
		err = s.Conn.Commit(ctx)
		if err != nil {
			result = false
			return
		}
	}(ctx)

	r := conn.QueryRow(ctx, `SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = $1)`, tableName)
	if err := r.Scan(&result); err != nil {
		return false
	}

	return result
}

func (s BulkService) ApplyDataVersion(ctx context.Context, dataVersionId uint, modelCode string) (updated []map[string]interface{}, deleted []map[string]interface{}, err error) {
	conn, ctx, err := s.Conn.Begin(ctx)
	defer func(ctx context.Context) {
		if err != nil {
			s.Conn.Rollback(ctx)
			return
		}
		err = s.Conn.Commit(ctx)
		if err != nil {
			err = errors.Wrap(err, "ApplyDataVersion commit")
			return
		}
	}(ctx)

	err = s.versionExist(ctx, dataVersionId)
	if err != nil {
		return nil, nil, errors.Wrap(InvalidArgument, "Version not found")
	}

	if modelCode == "" {
		return nil, nil, errors.Wrap(InvalidArgument, "Model codes are not specified")
	}

	if !s.isTableExist(ctx, modelCode) {
		return nil, nil, errors.Wrap(InvalidArgument, fmt.Sprintf("Table %v is not exist", modelCode))
	}

	tableParams := s.tableParams[modelCode]
	tableNameNew := tableParams.tableNameNew(dataVersionId)
	sequenceNameNew := tableParams.sequenceNameNew(dataVersionId)
	sequenceForAlignmentName := tableParams.sequenceForAlignmentName(dataVersionId)
	sequenceVersionBulkName := tableParams.sequenceVersionBulkName(dataVersionId)
	if !s.isTableExist(ctx, tableNameNew) && !s.isTableExist(ctx, tableParams.tableName) {
		return nil, nil, errors.Wrap(InvalidArgument, "Version for entity not found")
	}

	deletedColName := tableParams.DeletedColumnName()
	updatedColName := tableParams.UpdatedColumnName()

	var updateCount int
	r := conn.QueryRow(ctx, fmt.Sprintf(`SELECT COUNT(*) FROM %v WHERE %v = true OR %v = true`, tableNameNew, updatedColName, deletedColName))
	if err := r.Scan(&updateCount); err != nil {
		return nil, nil, err
	}

	if updateCount == 0 {
		s.Revert(ctx, dataVersionId, []string{modelCode})
		return nil, nil, nil
	}

	// название сиквенса ищем до удаления таблицы, т.к. после удаления таблицы ничего не вернется, но при этому
	// сам сиквенс еще будет существовать
	sequenceName := s.getSequenceName(ctx, conn, tableParams.tableName, tableParams.pkColumn)
	originalUniqueConstraint, err := s.Conn.GetConstraintName(ctx, tableParams.tableName, tableParams.uniqueConstraint)
	if err != nil {
		return nil, nil, err
	}

	//Удаляем все записи из временной таблицы со значением в колонке true и возвращаем их.
	del, err := conn.Query(ctx, fmt.Sprintf(`DELETE FROM %v WHERE %v = true RETURNING *`, tableNameNew, deletedColName))
	if err != nil {
		return nil, nil, err
	}
	deleted, err = s.prepareResultRows(del, modelCode)
	if err != nil {
		return nil, nil, err
	}

	//Возвращаем только обнолвенные записи из временной таблицы со значением в колонке = true.
	upd, err := conn.Query(ctx, fmt.Sprintf(`SELECT * FROM %v WHERE %v = true`, tableNameNew, updatedColName))
	if err != nil {
		return nil, nil, err
	}

	updated, err = s.prepareResultRows(upd, modelCode)
	if err != nil {
		return nil, nil, err
	}

	// удаляем старую таблицу
	_, err = conn.Exec(ctx, fmt.Sprintf("DROP TABLE %v;", tableParams.tableName))
	if err != nil {
		return nil, nil, errors.Wrap(err, "ApplyDataVersion: Drop old table")
	}

	//удаляем колонки deleted
	_, err = conn.Exec(ctx, fmt.Sprintf("ALTER TABLE %v DROP %v;", tableNameNew, deletedColName))
	if err != nil {
		return nil, nil, errors.Wrap(err, "ApplyDataVersion: Drop delete col")
	}

	//удаляем колонки updated
	_, err = conn.Exec(ctx, fmt.Sprintf("ALTER TABLE %v DROP %v;", tableNameNew, updatedColName))
	if err != nil {
		return nil, nil, errors.Wrap(err, "ApplyDataVersion: Drop delete col")
	}

	// удаляем колонку версии, создаем индекс первичного ключа
	_, err = conn.Exec(ctx, fmt.Sprintf("ALTER TABLE %v DROP %v;", tableNameNew, bulkVersionField))
	if err != nil {
		return nil, nil, errors.Wrap(err, "ApplyDataVersion: Drop bulk ver col")
	}

	if tableParams.pkColumn != "" {
		_, err = conn.Exec(ctx, fmt.Sprintf("ALTER TABLE %v ADD CONSTRAINT %v PRIMARY KEY (%v);", tableNameNew, tableParams.pkName(), tableParams.pkColumn))
		if err != nil {
			return nil, nil, errors.Wrap(err, "ApplyDataVersion: Drop ADD PK CONSTRAINT ")
		}
	}
	// переименовывем новую таблицу (заменяет собой старую таблицу), убираем значение по-умолчанию для первичного ключа
	_, err = conn.Exec(ctx, fmt.Sprintf("ALTER TABLE %v RENAME TO %v;", tableNameNew, tableParams.tableName))
	if err != nil {
		return nil, nil, errors.Wrap(err, "ApplyDataVersion: Remame ")
	}

	if tableParams.pkColumn != "" {
		_, err = conn.Exec(ctx, fmt.Sprintf("ALTER TABLE %v ALTER COLUMN %v DROP DEFAULT;", tableParams.tableName, tableParams.pkColumn))
		if err != nil {
			return nil, nil, errors.Wrap(err, "ApplyDataVersion")
		}

		_, err = conn.Exec(ctx, fmt.Sprintf("ALTER TABLE %v ALTER %v SET NOT NULL;", tableParams.tableName, tableParams.pkColumn))
		if err != nil {
			return nil, nil, errors.Wrap(err, "ApplyDataVersion")
		}
	}

	// удаляем сиквенсы старой таблицы. Если сиквенс окажется serial, то она удалится в момент удаления таблицы
	// и вместо названия сиквенса вернется пустая строка
	if sequenceName != "" {
		_, err = conn.Exec(ctx, fmt.Sprintf("DROP SEQUENCE IF EXISTS  %v;", sequenceName))
		if err != nil {
			return nil, nil, errors.Wrap(err, "ApplyDataVersion")
		}
	}

	if tableParams.pkColumn != "" {
		// удаляем сиквенсы: новой таблицы, батчинга
		_, err = conn.Exec(ctx, fmt.Sprintf("DROP SEQUENCE %v;", sequenceNameNew))
		if err != nil {
			return nil, nil, errors.Wrap(err, "ApplyDataVersion")
		}
	}

	_, err = conn.Exec(ctx, fmt.Sprintf("DROP SEQUENCE %v;", sequenceVersionBulkName))
	if err != nil {
		return nil, nil, errors.Wrap(err, "ApplyDataVersion")
	}

	if tableParams.pkColumn != "" {
		// переименовываем индекс и сиквенс
		_, err = conn.Exec(ctx, fmt.Sprintf("ALTER INDEX %v RENAME TO %v;", tableParams.uniqueConstraintNew(dataVersionId), originalUniqueConstraint))
		if err != nil {
			return nil, nil, errors.Wrap(err, "ApplyDataVersion")
		}

		_, err = conn.Exec(ctx, fmt.Sprintf("ALTER SEQUENCE %v RENAME TO %v;", sequenceForAlignmentName, tableParams.sequenceName()))
		if err != nil {
			return nil, nil, errors.Wrap(err, "ApplyDataVersion")
		}

		// устанавливаем таблице новый сиквенс (в котором ID идут по порядку без разрывов)
		_, err = conn.Exec(ctx, fmt.Sprintf("ALTER TABLE %v ALTER COLUMN %v SET DEFAULT nextval('%v');", tableParams.tableName, tableParams.pkColumn, tableParams.sequenceName()))
		if err != nil {
			return nil, nil, errors.Wrap(err, "ApplyDataVersion")
		}
	}

	return
}

func (s BulkService) prepareResultRows(rows pgx.Rows, modelCode string) (result []map[string]interface{}, err error) {
	tableParams := s.tableParams[modelCode]
	deletedColName := tableParams.DeletedColumnName()
	updatedColName := tableParams.UpdatedColumnName()
	descr := rows.FieldDescriptions()
	res := make(map[string]interface{}, len(descr))
	for rows.Next() {
		val, err := rows.Values()
		if err != nil {
			return nil, err
		}

		for i := range descr {
			n := string(descr[i].Name)
			if n == "data_version_bulk" || n == updatedColName || n == deletedColName {
				continue
			}
			res[n] = val[i]
		}
		result = append(result, res)
	}

	defer rows.Close()

	return result, nil
}

func (s BulkService) Revert(ctx context.Context, dataVersionId uint, modelCodes []string) (err error) {
	conn, ctx, err := s.Conn.Begin(ctx)
	defer func(ctx context.Context) {
		if err != nil {
			s.Conn.Rollback(ctx)
			return
		}
		err = s.Conn.Commit(ctx)
		if err != nil {
			err = errors.Wrap(err, "Revert commit")
			return
		}
	}(ctx)

	if len(modelCodes) == 0 {
		return errors.Wrap(InvalidArgument, "Model codes are not specified")
	}

	for _, modelCode := range modelCodes {
		tableParams := s.tableParams[modelCode]
		tableNameNew := tableParams.tableNameNew(dataVersionId)
		sequenceNameNew := tableParams.sequenceNameNew(dataVersionId)
		sequenceVersionBulkName := tableParams.sequenceVersionBulkName(dataVersionId)
		sequenceForAlignmentName := tableParams.sequenceForAlignmentName(dataVersionId)
		if !s.isTableExist(ctx, tableNameNew) {
			continue
		}

		_, err = conn.Exec(ctx, fmt.Sprintf(`DROP TABLE %v`, tableNameNew))
		if err != nil {
			return
		}

		if tableParams.pkColumn != "" {
			_, err = conn.Exec(ctx, fmt.Sprintf(`DROP SEQUENCE %v`, sequenceNameNew))
			if err != nil {
				return
			}
			_, err = conn.Exec(ctx, fmt.Sprintf(`DROP SEQUENCE %v`, sequenceVersionBulkName))
			if err != nil {
				return
			}
			_, err = conn.Exec(ctx, fmt.Sprintf(`DROP SEQUENCE %v`, sequenceForAlignmentName))
			if err != nil {
				return
			}
		}
	}

	return err
}

func (s BulkService) versionExist(ctx context.Context, id uint) error {
	conn, ctx, err := s.Conn.Begin(ctx)
	defer func(ctx context.Context) {
		if err != nil {
			s.Conn.Rollback(ctx)
			return
		}
		err = s.Conn.Commit(ctx)
		if err != nil {
			err = errors.Wrap(err, "versionExist commit")
			return
		}
	}(ctx)

	version := new(DataVersion)
	err = conn.QueryRow(ctx, `SELECT data_version_id FROM data_version WHERE data_version_id = $1`, id).Scan(&version.DataVersionId)

	return err
}

func (s BulkService) clearRepeat(models []interface{}, paramsName string) (rModels []interface{}) {

	keys := make(map[string]bool)
	tableParams := s.tableParams[paramsName]

	if len(models) > 0 {
		for _, item := range models {
			obj := s.mapFromInterface(item, tableParams)

			tkey := make([]string, 0)
			for _, conctrain := range tableParams.uniqueConstraint {
				if v, ok := obj[conctrain]; ok {
					tkey = append(tkey, fmt.Sprintf("%v", v))
				}
			}
			key := strings.Join(tkey, "/%/")

			if _, ok := keys[key]; !ok {
				keys[key] = true
				rModels = append(rModels, item)
			}
		}
	}

	return
}

func (s BulkService) splitPK(models []interface{}, paramsName string) (idModels []interface{}, Models []interface{}) {
	tableParams := s.tableParams[paramsName]

	if len(models) > 0 {
		for _, item := range models {
			obj := s.mapFromInterface(item, tableParams)

			if v, ok := obj[tableParams.pkColumn]; ok && v != nil {
				idModels = append(idModels, item)
			} else {
				Models = append(Models, item)
			}
		}
	}

	return
}

func (s BulkService) InsertDataVersion(ctx context.Context, dataVersionId uint, models []interface{}, paramsName string) (err error) {
	_, ctx, err = s.Conn.Begin(ctx)
	defer func(ctx context.Context) {
		if err != nil {
			s.Conn.Rollback(ctx)
			return
		}
		err = s.Conn.Commit(ctx)
		if err != nil {
			err = errors.Wrap(err, "InsertDataVersion commit")
			return
		}
	}(ctx)

	err = s.versionExist(ctx, dataVersionId)
	if err != nil {
		err = errors.Wrap(InvalidArgument, err.Error())
		return
	}
	models = s.clearRepeat(models, paramsName)

	tableParams := s.tableParams[paramsName]

	if _, err = s.createBulkModels(ctx, models, dataVersionId, tableParams); err != nil {
		err = errors.Wrap(Unknown, err.Error())
		return
	}

	return
}

func (s BulkService) unmarshalModels(rows pgx.Rows, successfulModels *Models) (err error) {
	fields := rows.FieldDescriptions()
	for rows.Next() {
		args := make([]interface{}, len(fields))
		argsLink := make([]interface{}, len(fields))
		for i := 0; i < len(fields); i++ {
			argsLink[i] = &args[i]
		}
		err := rows.Scan(argsLink...)
		if err == nil {
			model := Model{}
			for i := range fields {
				model[string(fields[i].Name)] = args[i]
			}
			*successfulModels = append(*successfulModels, model)
		}
	}

	return
}

//Возвращает стартовое значение сиквенса. Стартовое значение будет неизменным даже после вызова nextval()
func (s BulkService) getSequenceStartValue(ctx context.Context, sequenceName string) (val *string, err error) {
	conn, ctx, err := s.Conn.Begin(ctx)
	defer func(ctx context.Context) {
		if err != nil {
			s.Conn.Rollback(ctx)
			return
		}
		err = s.Conn.Commit(ctx)
		if err != nil {
			err = errors.Wrap(err, "getSequenceStartValue commit")
			return
		}
	}(ctx)

	q := "SELECT start_value as val  FROM information_schema.sequences WHERE CAST(sequence_name AS text) = $1"
	conn.QueryRow(ctx, q, sequenceName).Scan(&val)

	return
}

// Возвращает название сериала
func (s BulkService) getSequenceName(ctx context.Context, db pgx.Tx, tableName, pkColumnName string) string {
	columnDefault := ""

	// сначала ищем сериал
	db.QueryRow(ctx, "SELECT column_default as name from information_schema.columns where table_name=$1 AND column_name=$2;", tableName, pkColumnName).Scan(&columnDefault)
	name := strings.TrimPrefix(columnDefault, "nextval('")
	name = strings.TrimSuffix(name, "'::regclass)")

	return name
}

//  Добавляет или изменяет записи
func (s BulkService) createBulkModels(ctx context.Context, items []interface{}, dataVersionId uint, tableParam tableBulkParam) (dataVersionBulk uint, err error) {
	conn, ctx, err := s.Conn.Begin(ctx)
	defer func(ctx context.Context) {
		if err != nil {
			s.Conn.Rollback(ctx)
			return
		}
		err = s.Conn.Commit(ctx)
		if err != nil {
			err = errors.Wrap(err, "createBulkModels commit")
			return
		}
	}(ctx)

	err = s.versionExist(ctx, dataVersionId)
	if err != nil {
		return 0, errors.Wrap(InvalidArgument, "Version not found")
	}

	tableNameNew := tableParam.tableNameNew(dataVersionId)
	if !s.isTableExist(ctx, tableNameNew) {
		return 0, fmt.Errorf("error bulk insert: table %v does not exist", tableParam.tableName)
	}

	sequenceVersionBulkName := tableParam.sequenceVersionBulkName(dataVersionId)
	err = conn.QueryRow(ctx, fmt.Sprintf(`SELECT nextval('%v') as max`, sequenceVersionBulkName)).Scan(&dataVersionBulk)

	uniqueConstraint, err := s.Conn.GetConstraintName(ctx, tableNameNew, tableParam.uniqueConstraint)
	if err != nil {
		return 0, err
	}
	err = s.batchInsert(ctx, items, tableNameNew, uniqueConstraint, dataVersionBulk, tableParam)

	return dataVersionBulk, err
}

func (s BulkService) fieldsFromInterface(item interface{}, tableParam tableBulkParam, fields *[]string) {
	ts := reflect.TypeOf(item)
	el := reflect.ValueOf(item)

	if k := el.Kind(); k == reflect.Ptr {
		ts = ts.Elem()
		el = el.Elem()
	}

	if k := el.Kind(); k != reflect.Struct && k != reflect.Interface {
		return
	}

	for i := 0; i < ts.NumField(); i++ {
		if columnName, ok := tableParam.fields[ts.Field(i).Name]; ok {
			*fields = append(*fields, columnName)
		}
	}

	return
}

func (s BulkService) mapFromInterface(item interface{}, tableParam tableBulkParam) (obj map[string]interface{}) {
	itemType := reflect.TypeOf(item)
	itemVal := reflect.ValueOf(item)

	if k := itemVal.Kind(); k == reflect.Ptr {
		itemType = itemType.Elem()
		itemVal = itemVal.Elem()
	}

	if k := itemVal.Kind(); k != reflect.Struct && k != reflect.Interface {
		return
	}

	obj = make(map[string]interface{})
	for i := 0; i < itemType.NumField(); i++ {
		if columnName, ok := tableParam.fields[itemType.Field(i).Name]; ok {
			v := itemVal.Field(i)
			if isEmptyValue(v) {
				continue
			}
			obj[columnName] = v.Interface()
		}
	}

	return
}

func (s BulkService) batchInsert(ctx context.Context, items []interface{}, tableName, uniqueConstraintName string, dataVersionBulk uint, tableParam tableBulkParam) error {

	if len(items) == 0 {
		return nil
	}

	conn, ctx, err := s.Conn.Begin(ctx)
	defer func(ctx context.Context) {
		if err != nil {
			s.Conn.Rollback(ctx)
			return
		}
		err = s.Conn.Commit(ctx)
		if err != nil {
			err = errors.Wrap(err, "batchInsert commit")
			return
		}
	}(ctx)

	qfields, values, replace, attrs, _ := s.getInsertQueryParams(items, tableParam, dataVersionBulk)
	query := fmt.Sprintf(
		`INSERT INTO
			"%s" as p (%s) VALUES %s
			ON CONFLICT ON CONSTRAINT %v 
			DO UPDATE SET %v`,
		tableName,
		strings.Join(qfields, ", "),
		strings.Join(values, ", "),
		uniqueConstraintName,
		strings.Join(replace, ", "),
	)

	_, err = conn.Exec(ctx, query, attrs...)
	if err != nil {
		return err
	}

	return nil
}

func PrepareQueryWhere(query string) string {
	filter := ""
	result := strings.Split(query, "&")

	if result[0] == "" {
		return filter
	}

	filter = "WHERE "
	l := len(result)
	for i := range result {
		q := strings.Split(result[i], "=")
		filter += fmt.Sprintf(`%v='%v'`, q[0], q[1])
		if l-1 != i {
			filter += " AND "
		}
	}

	return filter
}

func (s BulkService) getInsertQueryParams(
	items []interface{},
	tableParam tableBulkParam,
	dataVersionBulk uint,
) (qfields []string, values []string, replace []string, attrs []interface{}, conditions []string) {
	fields := make([]string, 0)
	if dataVersionBulk != 0 {
		fields = append(fields, bulkVersionField, tableParam.UpdatedColumnName(), tableParam.DeletedColumnName())
	}

	s.fieldsFromInterface(items[0], tableParam, &fields)
	qfields = make([]string, len(fields))
	for i, f := range fields {
		qfields[i] = "\"" + f + "\""
	}

	values = make([]string, len(items))
	attrs = make([]interface{}, 0)
	n := 1
	for num, item := range items {
		obj := s.mapFromInterface(item, tableParam)
		vals := make([]string, 0)

		if dataVersionBulk != 0 {
			vals = append(vals, fmt.Sprintf("$%v", n))
			attrs = append(attrs, dataVersionBulk)
			n++
		}

		for _, f := range fields {
			if dataVersionBulk != 0 && f == bulkVersionField {
				continue
			}

			if f == tableParam.DeletedColumnName() {
				attrs = append(attrs, false)
				vals = append(vals, fmt.Sprintf("$%v", n))
				n++
				continue
			}

			if f == tableParam.UpdatedColumnName() {
				attrs = append(attrs, true)
				vals = append(vals, fmt.Sprintf("$%v", n))
				n++
				continue
			}

			if value, ok := obj[f]; ok {
				attrs = append(attrs, value)
				vals = append(vals, fmt.Sprintf("$%v", n))
				n++
			} else {
				vals = append(vals, "default")
			}
		}

		values[num] = "(" + strings.Join(vals, ", ") + ")"
	}

	conditions = make([]string, 0, len(fields))
	for _, v := range fields {
		if v == "data_version_bulk" || v == tableParam.pkColumn || v == tableParam.DeletedColumnName() || v == tableParam.UpdatedColumnName() {
			continue
		}

		conditions = append(conditions, fmt.Sprintf("p.%v != EXCLUDED.%v", v, v))
	}

	replace = make([]string, 0, len(fields))
	for _, v := range fields {
		if v == tableParam.pkColumn {
			continue
		}
		if v == tableParam.UpdatedColumnName() {
			replace = append(replace, fmt.Sprintf("%v = (%v)", tableParam.UpdatedColumnName(), strings.Join(conditions, " OR ")))
			continue
		}
		replace = append(replace, fmt.Sprintf("%v = EXCLUDED.%v", v, v))
	}

	return
}

// Insert - fast and unsafe insert, insert items in exist table, return inserting items
func (s BulkService) Insert(ctx context.Context, items []interface{}, paramsName string) (successfulModels Models, err error) {
	// If there is no data, nothing to do.
	if len(items) == 0 {
		return
	}

	tableParam, ok := s.tableParams[paramsName]
	if !ok {
		return
	}

	items = s.clearRepeat(items, paramsName)
	idModels, items := s.splitPK(items, paramsName)

	if len(idModels) > 0 {
		if err = s.insert(ctx, idModels, tableParam, &successfulModels, []string{tableParam.pkColumn}); err != nil {
			return
		}
	}

	if len(items) > 0 {
		if err = s.insert(ctx, items, tableParam, &successfulModels, tableParam.uniqueConstraint); err != nil {
			return
		}
	}

	return
}

func (s BulkService) insert(ctx context.Context, items []interface{}, tableParam tableBulkParam, successfulModels *Models, uniqueConstraint []string) (err error) {
	conn, ctx, err := s.Conn.Begin(ctx)
	defer func(ctx context.Context) {
		if err != nil {
			s.Conn.Rollback(ctx)
			return
		}
		err = s.Conn.Commit(ctx)
		if err != nil {
			err = errors.Wrap(err, "insert commit")
			return
		}
	}(ctx)

	f, v, r, attrs, conditions := s.getInsertQueryParams(items, tableParam, 0)

	uniqueConstraintName, err := s.Conn.GetConstraintName(ctx, tableParam.tableName, uniqueConstraint)
	if err != nil {
		return err
	}

	sql := fmt.Sprintf("INSERT INTO \"%s\" (%s) VALUES %s ON CONFLICT ON CONSTRAINT %v DO UPDATE SET %v WHERE %v RETURNING *;",
		tableParam.tableName,
		strings.Join(f, ", "),
		strings.Join(v, ", "),
		uniqueConstraintName,
		strings.Join(r, ", "),
		strings.Join(conditions, " OR "),
	)

	rows, err := conn.Query(ctx, sql, attrs...)
	if err != nil {
		return errors.Wrap(Unknown, err.Error())
	}

	err = s.unmarshalModels(rows, successfulModels)
	if err != nil {
		return errors.Wrap(Unknown, err.Error())
	}
	return
}

func isEmptyValue(v reflect.Value) bool {
	switch v.Kind() {
	case reflect.Array, reflect.Map, reflect.Slice, reflect.String:
		return v.Len() == 0
	case reflect.Bool:
		return !v.Bool()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return v.Int() == 0
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return v.Uint() == 0
	case reflect.Float32, reflect.Float64:
		return v.Float() == 0
	case reflect.Interface, reflect.Ptr:
		return v.IsNil()
	}
	return false
}

func (s BulkService) loop(ctx context.Context) {
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM, syscall.SIGQUIT)

	clearTick := time.Tick(time.Hour * time.Duration(s.clearDelay))

	for {
		select {
		case <-clearTick:
			if err := s.cleaner(); err != nil {
				fmt.Println(err)
			}
		case <-quit:
			break
		}
	}
}

// garbage collector
func (s BulkService) cleaner() (err error) {
	conn, err := s.Conn.GetMasterConn(s.ctx)
	if err != nil {
		return
	}
	defer conn.Release()

	// Находим все записи старше определённого срока
	query := `
		UPDATE public.data_version
		SET delete = true
		WHERE time < $1
		  AND delete = false
		RETURNING data_version_id`
	rows, err := conn.Query(s.ctx, query, time.Now().Add(-(time.Hour * time.Duration(s.clearDelay))))
	if err != nil {
		return
	}

	defer func() {
		rows.Close()
	}()

	for rows.Next() {
		dataVersionID := uint(0)

		err := rows.Scan(&dataVersionID)
		if err != nil {
			continue
		}

		err = s.clean(dataVersionID)
		if err != nil {
			_, _ = conn.Exec(s.ctx, `UPDATE public.data_version SET delete = false WHERE data_version_id = $1`, dataVersionID)
		}
	}

	return
}

// Удаляет все доступные таблици и запись о операции
func (s BulkService) clean(dataVersionId uint) (err error) {
	conn, err := s.Conn.GetMasterConn(s.ctx)
	if err != nil {
		return
	}
	defer conn.Release()

	// список доступных таблиц
	mc := make([]string, 0)
	for s := range s.tableParams {
		mc = append(mc, s)
	}

	// Удаляем все временные таблицы
	if err := s.Revert(s.ctx, dataVersionId, mc); err != nil {
		return err
	}

	// удаляем версию
	_, _ = conn.Exec(s.ctx, `DELETE FROM data_version WHERE data_version_id = $1`, dataVersionId)

	return
}
