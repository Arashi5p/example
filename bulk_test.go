package bulk

import (
	"context"
	"database/sql"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	pgx_connection "gitlab.eldorado.ru/golang/pgx-connect"

	_ "github.com/lib/pq"
)

var databaseUrl = "postgres://postgres:123456@localhost:5432/bulk?sslmode=disable"

type City struct {
	CityId     uint32    `json:"city_id,omitempty"`
	Name       string    `json:"name,omitempty"`
	RegionId   int       `json:"region_id,omitempty"`
	Comment    string    `json:"comment,omitempty"`
	DateCreate time.Time `jsom:"date_create"`
}

var cities = []*City{
	{
		Name:       "test1",
		CityId:     3,
		Comment:    "test",
		DateCreate: time.Now(),
	},
	{
		Name:       "test2",
		CityId:     33,
		Comment:    "test",
		DateCreate: time.Now(),
	},
	{
		Name:       "test3",
		CityId:     34,
		Comment:    "test",
		DateCreate: time.Now(),
	},
	{
		Name:    "test4",
		CityId:  35,
		Comment: "test",
	},
	{
		Name:       "test5",
		CityId:     36,
		DateCreate: time.Now(),
	},
	{
		Name:       "test6",
		CityId:     37,
		Comment:    "test",
		DateCreate: time.Now(),
	},
	{
		Name:       "test7",
		CityId:     38,
		Comment:    "test",
		DateCreate: time.Now(),
	},
	{
		Name:       "test8",
		CityId:     39,
		Comment:    "test",
		DateCreate: time.Now(),
	},
	{
		Name:       "msk",
		CityId:     123456,
		Comment:    "test",
		DateCreate: time.Now(),
	},
	{
		Name:       "asd",
		CityId:     1234567,
		Comment:    "test",
		DateCreate: time.Now(),
	},
	{
		Name:       "msk",
		CityId:     123456,
		Comment:    "test",
		DateCreate: time.Now(),
	},
	{
		Name:       "as11d",
		CityId:     1234567,
		Comment:    "test",
		DateCreate: time.Now(),
	},
	{
		Name:       "m333sk",
		CityId:     78867786,
		Comment:    "test",
		DateCreate: time.Now(),
	},
	{
		Name:       "333",
		CityId:     8567,
		Comment:    "test",
		DateCreate: time.Now(),
	},
	{
		Name:       "mфывфsk",
		CityId:     123456564,
		Comment:    "test",
		DateCreate: time.Now(),
	},
	{
		Name:       "aафаsd",
		CityId:     6567,
		Comment:    "test",
		DateCreate: time.Now(),
	},
}

func TestInit(t *testing.T) {

	db, err := sql.Open("postgres", databaseUrl)
	assert.NoError(t, err)
	db.Exec(`CREATE TABLE IF NOT EXISTS bulk_test
					(
						name       VARCHAR(8),
						city_id    INT,
						created_at TIMESTAMP DEFAULT now(),
						comment    VARCHAR(240) NULL,
						CONSTRAINT constraint_name_unique UNIQUE (name),
						CONSTRAINT constraint_city_id_pk PRIMARY KEY (city_id)
					)`)
}

func TestBulkLifeCycle(t *testing.T) {
	ctx := context.Background()
	conn, err := pgx_connection.NewConnect(ctx, &pgx_connection.Config{
		Master: &pgx_connection.Database{
			DatabaseName: "bulk",
			User:         "postgres",
			Password:     "123456",
			Host:         "localhost",
			Port:         5432,
			Secure:       "disable",
		},
		Replica: nil,
	})
	assert.NoError(t, err)

	src, err := NewBulkService(Config{
		TableBulkParams: []TableParam{
			{
				Name:     "bulk_test",
				PkColumn: "city_id",
				Fields: map[string]string{
					"Name":      "name",
					"CityId":    "city_id",
					"Comment":   "comment",
					"CreatedAt": "created_at",
				},
				UniqueConstraint: []string{"name"},
			},
		},
	}, conn)

	assert.NoError(t, err)
	assert.NotEmpty(t, src)

	CreateDataVersion(ctx, src, &CreateDataVersionRequest{
		"bulk_test", "",
	})
	assert.NoError(t, err)

	models := make([]interface{}, 0)
	for _, v := range cities {
		models = append(models, v)
	}
	//err = src.InsertDataVersion(ctx, uint(dv.DataVersionId), models, "bulk_test")
	//assert.NoError(t, err)
	//
	//_, _, err = src.ApplyDataVersion(ctx, uint(dv.DataVersionId), "bulk_test")
	//assert.NoError(t, err)
}

type CreateDataVersionRequest struct {
	Code        string `json:"code,omitempty"`
	DeleteQuery string `json:"DeleteQuery,omitempty"`
}

type DataVersionTest struct {
	DataVersionId uint32 `json:"dataVersionId,omitempty"`
}

func CreateDataVersion(ctx context.Context, src *BulkService, req *CreateDataVersionRequest) (resp *DataVersionTest, err error) {
	dv, err := src.CreateDataVersion(ctx, req.Code, req.DeleteQuery)
	if err != nil {
		return nil, err
	}
	return &DataVersionTest{DataVersionId: dv.DataVersionId}, err
}

func TestBulkService_CreateDataVersion(t *testing.T) {

	version := DataVersion{}
	db, err := sql.Open("postgres", databaseUrl)
	assert.NoError(t, err)
	assert.NotNil(t, db)

	rows, err := db.Query(`SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = $1)`, "data_version")
	assert.NoError(t, err)

	var tableExists bool
	rows.Scan(&tableExists)
	assert.False(t, tableExists)

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS data_version(data_version_id serial PRIMARY KEY,"time" timestamp without time zone,"delete" boolean DEFAULT false);`)
	assert.NoError(t, err)

	rows, err = db.Query(`INSERT INTO data_version (time) VALUES ($1) RETURNING data_version_id`, time.Now())
	assert.NoError(t, err)
	rows.Scan(&version.DataVersionId)
	assert.NotNil(t, version.DataVersionId)

}

func TestFinal(t *testing.T) {
	db, err := sql.Open("postgres", databaseUrl)
	assert.NoError(t, err)
	db.Exec(`DROP TABLE IF EXISTS bulk_test`)
}

func TestPrepareQueryWhere(t *testing.T) {
	mock := "name=msk&city_id=123456"
	filter := `WHERE name='msk' AND city_id='123456'`
	result := PrepareQueryWhere(mock)
	assert.NotEmpty(t, result)
	assert.Equal(t, filter, result)
}
