module gitlab.eldorado.ru/golang/pgx-connect

go 1.15

require (
	github.com/jackc/pgx/v4 v4.10.1
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.7.0
)
