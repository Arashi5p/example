# pgx-connect

Сервис для подключения к базе данных, средствами пакета pgx.  

Пример использования

##### Создание сервиса
```
conn, err := NewConnect(ctx, &pgx_connection.Config{
		Master: &Database{
			DatabaseName: "test",
			User:         "postgres",
			Password:     "postgres",
			Host:         "localhost",
			Port:         5432,
			Secure:       "disable",
		},
		Replica: &Database{
			DatabaseName: "test",
			User:         "postgres",
			Password:     "postgres",
			Host:         "localhost",
			Port:         5432,
			Secure:       "disable",
		},
	})
```
##### Старт транзакции
```
pgxPool, ctx, err := conn.Begin(ctx)
defer func(ctx context.Context) {
    if err == nil {conn.Commit(ctx)} else {conn.Rollback(ctx)}
}(ctx)
	
	
```

##### Коммит
```
ctx, err = conn.Commit(ctx)
```

##### Роллбэк
```
ctx, err = conn.Rollback(ctx)
```
##### Особенность работы с pgx.Row:

если используется метод Query возвращающий pgx.Rows, 
запрос необходимо закрыть, иначе соединение будет занято
```
conn, ctx, err := conn.Begin(ctx)
r, err := conn.Query(ctx, "SELECT 1")
r.Close()
```

