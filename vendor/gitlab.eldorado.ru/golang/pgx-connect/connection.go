package pgx_connection

import (
	"context"
	"fmt"
	"sync/atomic"

	"github.com/jackc/pgx/v4"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

const (
	Conn              string = "Conn"
	TimeStampLayout   string = "15:04:05"
	ConstraintNameSQL        = `SELECT kcu1.constraint_name
	FROM information_schema.key_column_usage as kcu1
	WHERE kcu1.table_name = $1 AND kcu1.constraint_name IN (
		  SELECT DISTINCT kcu.constraint_name
		  FROM information_schema.key_column_usage as kcu
		  WHERE kcu.table_name = $1 AND kcu.column_name = ANY ($2)
	)
	GROUP BY kcu1.constraint_name
	HAVING COUNT(kcu1.constraint_name) = $3`
)

// Структура для хранения коннектов к мастеру и реплике
type connection struct {
	Master  *pgxpool.Pool
	Replica *pgxpool.Pool
}

type Config struct {
	Master  *Database
	Replica *Database
}

type Database struct {
	User         string
	Password     string
	Host         string
	Port         int
	DatabaseName string
	Secure       string
}

type transaction struct {
	tx    pgx.Tx
	conn  *pgxpool.Conn
	count uint32
}

type Connection interface {
	Ping(ctx context.Context) error
	GetMasterConn(context.Context) (*pgxpool.Conn, error)
	GetReplicaConn(context.Context) (*pgxpool.Conn, error)
	GetConstraintName(context.Context, string, []string) (string, error)
	Close()
	Begin(ctx context.Context) (pgx.Tx, context.Context, error)
	Rollback(context.Context) error
	Commit(context.Context) error
}

func NewConnect(ctx context.Context, cfg *Config) (*connection, error) {
	var res connection
	var err error

	res.Master, err = res.conn(ctx, cfg.Master)
	if err != nil {
		return nil, errors.Wrap(err, "Master DB connect")
	}

	res.Replica, err = res.conn(ctx, cfg.Replica)
	if err != nil {
		return nil, errors.Wrap(err, "Replica DB connect")
	}

	return &res, nil
}

func (c *connection) Begin(ctx context.Context) (pgx.Tx, context.Context, error) {
	if ta, ok := ctx.Value(Conn).(*transaction); !ok || ta.count == 0 {
		conn, err := c.GetMasterConn(ctx)
		if err != nil {
			return nil, ctx, err
		}

		defer func() {
			if r := recover(); r != nil {
				conn.Release()
				err, _ = r.(error)
			}
		}()

		tx, err := conn.Begin(ctx)
		if err != nil {
			return nil, ctx, err
		}

		ta = new(transaction)
		ta.tx = tx
		ta.conn = conn
		atomic.AddUint32(&ta.count, 1)

		ctx = context.WithValue(ctx, Conn, ta)

		return tx, ctx, nil
	} else {
		if ta.count > 0 {
			atomic.AddUint32(&ta.count, 1)
			return ta.tx, ctx, nil
		}
	}

	return nil, ctx, errors.New("transaction not established")
}

func (t *connection) Rollback(ctx context.Context) error {
	ta, ok := ctx.Value(Conn).(*transaction)
	if !ok || ta.conn == nil {
		return nil
	}

	defer func() {
		ta.conn.Release()
		ta.conn = nil
		atomic.StoreUint32(&ta.count, 0)
	}()

	if err := ta.tx.Rollback(ctx); err != nil {
		return err
	}

	return nil
}

func (t *connection) Commit(ctx context.Context) error {
	ta, ok := ctx.Value(Conn).(*transaction)
	if !ok || ta.conn == nil {
		return nil
	}

	if ta.count > 1 {
		atomic.AddUint32(&ta.count, ^uint32(0))
		return nil
	}

	defer func() {
		ta.conn.Release()
		ta.conn = nil
		atomic.StoreUint32(&ta.count, 0)
	}()

	if err := ta.tx.Commit(ctx); err != nil {
		return err
	}

	return nil
}

func (c *connection) Ping(ctx context.Context) error {
	conn, err := c.Master.Acquire(ctx)
	if err != nil {
		return errors.Wrap(err, "master ping error")
	}
	conn.Release()

	conn, err = c.Replica.Acquire(ctx)
	if err != nil {
		return errors.Wrap(err, "replica ping error")
	}
	conn.Release()
	return nil
}

func (c *connection) Close() {
	c.Master.Close()
	c.Replica.Close()
}

func (r *connection) conn(ctx context.Context, db *Database) (c *pgxpool.Pool, err error) {
	if db == nil {
		return
	}

	return pgxpool.Connect(ctx, fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=%s",
		db.User,
		db.Password,
		db.Host,
		db.Port,
		db.DatabaseName,
		db.Secure,
	))
}

func (c *connection) GetMasterConn(ctx context.Context) (*pgxpool.Conn, error) {
	conn, err := c.Master.Acquire(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "get master connection")
	}
	return conn, nil
}

func (c *connection) getMasterConnPool() (*pgxpool.Pool, error) {
	conn := c.Master
	if conn != nil {
		return nil, errors.New("get master pool connection")
	}
	return conn, nil
}

func (c *connection) GetReplicaConn(ctx context.Context) (*pgxpool.Conn, error) {
	conn, err := c.Replica.Acquire(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "get replica connection")
	}
	return conn, nil
}

func (c *connection) GetConstraintName(ctx context.Context, tableName string, columnNames []string) (constraintName string, err error) {
	if len(columnNames) == 0 {
		return
	}

	tx, ctx, err := c.Begin(ctx)
	defer func(ctx context.Context) {
		if err != nil {
			c.Rollback(ctx)
			constraintName = ""
			return
		}
		err = c.Commit(ctx)
		if err != nil {
			err = errors.Wrap(err, "GetConstraintName commit")
			constraintName = ""
			return
		}
	}(ctx)

	err = tx.QueryRow(ctx, ConstraintNameSQL, tableName, columnNames, len(columnNames)).Scan(&constraintName)
	return
}
